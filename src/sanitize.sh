#!/bin/bash

sed -i.bak 's/Vesuviana. An international Journal of Archaeological and Historical Studies on Pompei and Herculaneum – Pisa, Roma/Vesuviana. An international Journal of Archaeological and Historical Studies on Pompei and Herculaneum - Pisa, Roma/' BAHR_REVUE.xml
sed -i.bak 's/Polis. Studi interdisciplinari sul mondo antico, Reggio di Calabria/Polis. Studi interdisciplinari sul mondo antico - Reggio di Calabria/' BAHR_REVUE.xml
sed -i.bak 's/Lalies : actes des sessions de linguistique et de littérature – Paris/Lalies : actes des sessions de linguistique et de littérature - Paris/' BAHR_REVUE.xml
sed -i.bak 's/Hessen Archäologie, Wiesbaden/Hessen Archäologie - Wiesbaden/' BAHR_REVUE.xml
sed -i.bak 's/ -Turnhout/ - Turnhout/' BAHR_REVUE.xml
sed -i.bak 's/Sicilia Antiqua: an international journal of archaeology – Pisa, Roma/Sicilia Antiqua: an international journal of archaeology - Pisa, Roma/' BAHR_REVUE.xml
#sed -i.bak 's///' BAHR_REVUE.xml
sed -i.bak 's/Jahrbuch der Schweizerischen Gesellschaft für Ur- und Frühgeschichte - Basel. Devient Jahrbuch Archäologie Schweiz - Basel<\/FIELD2>/Jahrbuch der Schweizerischen Gesellschaft für Ur- und Frühgeschichte - Basel<\/FIELD2><FIELD3>Devient Jahrbuch Archäologie Schweiz - Basel<\/FIELD3>/' BAHR_REVUE.xml
# créer historique pour:
# UNIQUE_ID 84 (l109)
# UNIQUE_ID 323 (l527)

# pour BAHR_OC, il y a des guillemets mal foutus dans certains titres
# en conséquence, c'est ';' qui est utilisé comme séparateur, et les valeurs ne sont pas délimitées de '"'
# par contre, pour qu'Heurist comprenne correctement, le header doit être harmonisé
