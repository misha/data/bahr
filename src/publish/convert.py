# -*- coding: utf-8 -*-
from lxml import etree
from cli import create_parser
from records import Record
import heurist, html, htmlz
from thesaurus import update_tree, output_tree


def parse_tree(path):
    tree = None
    with open(path, 'r', encoding='utf-8') as fd:
        tree = etree.parse(fd)#, parser)
    return tree


def unmarshall(context):
    def get_tag(e):  # we want to remove namespaces from hml tags
        return etree.QName(e.tag).localname
    target = Record()
    Record.get_tag = get_tag
    for action, e in context:
        res = getattr(target, action)(e)
    return target.records


def validate_records(records, record_type, target, errors, replace=True):
    results = list()
    for hid, fields in records[record_type].items():
        record = target(fields)
        record.validate(records, errors)
        results.append(record)
    if replace:
        for record in results:
            records[record_type][record.hid] = record
    return results

def unromanize(expr):
    if expr.strip().startswith(('ALA', 'LEGIO', 'COHORS')):
        words = expr.split(' ')
        result = []
        for word in words:
            try:
                from roman import roman2int
                x = roman2int(word)
                result.append(str(x).zfill(3))
            except ValueError as ex:
                result.append(word)
        return ' '.join(result)
    return expr


def write_html(config, records):

    IN_NOTICES_KEY = 'in_notices'
    BOOK_REF_KEY = 'bahr_book_ref'
    def add_book_reference(element, notice):
        try:
            references = getattr(element, IN_NOTICES_KEY)
        except AttributeError:
            references = []
        references.append(getattr(notice, BOOK_REF_KEY))
        setattr(element, IN_NOTICES_KEY, references)

    def generate_lexicon(records, rtype, field, retrieve=None):
        if not retrieve:
            retrieve = lambda notice, field: notice.get(field, [])

        elements = records[rtype]
        notices = records[heurist.Notice.RECORD_TYPE]
        # we sort notices before iterating over them, ...
        notices_sorted = list(notices.values())
        notices_sorted.sort(key=lambda x: getattr(x, BOOK_REF_KEY))
        for notice in notices_sorted:
            values = retrieve(notice, field)
            for hid in values:
                try:
                    e = elements[hid]
                    # ... so that when we add them, they are already sorted
                    add_book_reference(e, notice)
                except KeyError:
                    if field != 'sources':
                        print("Notice %s: '%s' not found in %s" % (notice.hid, hid, field))
                    # else it's a Source pointed directly by the Notice,
                    # instead of a Reference (which in turn points on a Source)

    def lexicon_as_html(elements, entry_formatter=None):
        def update_lexicon(lexicon, names, initial):
            if len(names) < 1:
                return
            lexicon.append('''<section><h4>{initial}</h4><div class='names'>{names}</div></section>'''
                    .format(initial=initial, names=''.join(names)))
        if not entry_formatter:
            entry_formatter = html.format_lexicon_entry

        lexicon = []
        names = []
        initial = htmlz.strip_diacritics(elements[0].initial.upper())
        for e in elements:
            new_initial = htmlz.strip_diacritics(e.initial.upper())
            if new_initial != initial:
                update_lexicon(lexicon, names, initial)
                names = []
                initial = new_initial
            references = getattr(e, IN_NOTICES_KEY, [])
            name = entry_formatter(e, references)
            if name:
                names.append(name)
        if len(elements) > 0:
            update_lexicon(lexicon, names, initial)
        return lexicon

    def update_sources_for_lexicon(records, FIELD_KEY):
        references = records[heurist.Reference.RECORD_TYPE]
        for hid, reference in references.items():
            if not reference.source:
                continue
            source = heurist.Reference.get_source(records, reference.source)
            source_refs = getattr(source, FIELD_KEY, set())
            source_refs.add(hid)
            setattr(source, FIELD_KEY, source_refs)
            #if len(source_refs) > 1:
            #    print('%s -> %s %s' % (reference, source, source_refs))
        # Sources.bahr_refs are now populated with H-ID of References.

    errors = []
    # LOAD KEYWORDS
    rtype = htmlz.Keyword.RECORD_TYPE
    themes = validate_records(records, rtype, htmlz.Keyword, errors)
    if config.thesaurus:
        roots, candidates, orphans = update_tree(records[rtype])
        with open(config.output, 'w') as fd:
            output_tree(fd, roots, candidates, orphans)
        return
    rtype = heurist.Anthroponyme.RECORD_TYPE
    gods = validate_records(records, rtype, htmlz.Anthroponyme, errors)
    rtype = heurist.Lieu.RECORD_TYPE
    places = validate_records(records, rtype, htmlz.Lieu, errors)
    rtype = heurist.Peuple.RECORD_TYPE
    people = validate_records(records, rtype, htmlz.Peuple, errors)

    # LOAD CONTRIBUTORS
    rtype = heurist.Signature.RECORD_TYPE
    signatures = validate_records(records, rtype, html.Signature, errors)
    signatures.sort(key=lambda x: x.nom)
    no_dups = {repr(s).lower(): s for s in signatures}
    contributeurs = []
    for s in no_dups.keys():
        contributeurs.append(no_dups[s].as_html(records))

    # LOAD AUTHORS
    rtype = heurist.Auteur.RECORD_TYPE
    authors = validate_records(records, rtype, htmlz.Auteur, errors)

    # LOAD SOURCES
    rtype = heurist.Source.RECORD_TYPE
    sources = validate_records(records, rtype, html.Source, errors)
    rtype = heurist.Reference.RECORD_TYPE
    references = validate_records(records, rtype, html.Reference, errors)

    # LOAD COLLECTIONS
    rtype = heurist.Collection.RECORD_TYPE
    validate_records(records, rtype, html.Collection, errors)
    # LOAD REVUES
    rtype = heurist.Revue.RECORD_TYPE
    revues = validate_records(records, rtype, htmlz.Revue, errors)
    def get_abbreviation(revue):
        abbr = revue.get('abbreviation')[0]
        return htmlz.strip_diacritics(abbr)
    revues.sort(key=lambda x: get_abbreviation(x).lower())
    # LOAD OC
    rtype = heurist.OuvrageCollectif.RECORD_TYPE
    ocs = validate_records(records, rtype, htmlz.OuvrageCollectif, errors)
    ocs.sort(key=lambda x: x.get_authors(records))

    # LOAD NOTICES
    rtype = heurist.Notice.RECORD_TYPE
    setattr(heurist.Notice, 'use_language_codes', config.language_codes)
    notices = validate_records(records, rtype, htmlz.Notice, errors)
    notices.sort(key=lambda x: x.get_sorting_string(records))
    # GENERATE PUBLISHING IDENTIFIERS
    counter = 1
    for notice in notices:
        setattr(notice, BOOK_REF_KEY, '%sA-%04d' % (config.year, counter))
        counter += 1

    # GENERATE LANGUAGES CODES TABLE
    languages = ''
    if config.language_codes:
        codes = [v[0] for v in heurist.Notice.LANGUAGES.values() if v[1]]
        codes.sort()
        def get_label(code):
            for label, values in heurist.Notice.LANGUAGES.items():
                if values[0] == code:
                    return label
            return '?'
        languages += '''
<section id="languages" style="margin-top: 40pt;">
  <h2 id="languages-title">Langues</h2>
  <ul>
                    '''
        for code in codes:
            languages += '''
    <li><span class="code">{code}</span>{label}</li>'''.format(
            code=code, label=get_label(code))
        languages += '''
  </ul>
</section>
'''

    # GENERATE LEXICONS
    generate_lexicon(records, heurist.Auteur.RECORD_TYPE, 'auteurs')
    generate_lexicon(records, heurist.Anthroponyme.RECORD_TYPE, 'anthroponymes')
    generate_lexicon(records, heurist.Peuple.RECORD_TYPE, 'peuples')
    generate_lexicon(records, heurist.Lieu.RECORD_TYPE, 'noms_geographiques')
    generate_lexicon(records, htmlz.Keyword.RECORD_TYPE, 'mots_clefs')
    #authors = records[heurist.Auteur.RECORD_TYPE].values()
    authors.sort(key=lambda x: htmlz.strip_diacritics(x.nom))
    authors = [a for a in authors if not a.et_al]  # filter "et alii" from lex
    authors_html = lexicon_as_html(authors)
    anthros = gods + people
    anthros.sort(key=lambda x: htmlz.strip_diacritics(x.descripteur.upper()))
    anthros_html = lexicon_as_html(anthros)
    places.sort(key=lambda x: htmlz.strip_diacritics(x.descripteur.upper()))
    places_html = lexicon_as_html(places)
    themes.sort(key=lambda x: unromanize(htmlz.strip_diacritics(x.descripteur.upper())))
    themes_html = lexicon_as_html(themes)

    # sources lexicon is a special case
    def get_sources_of_notice(notice, field):
        notice.update_sources(records)
        sources_with_or_without_refs = getattr(notice, field, {})
        return [source.hid for source in sources_with_or_without_refs]

    FIELD_KEY = 'bahr_refs'
    update_sources_for_lexicon(records, FIELD_KEY)
    generate_lexicon(records, heurist.Reference.RECORD_TYPE, 'sources')
    generate_lexicon(records, heurist.Source.RECORD_TYPE, 'references', retrieve=get_sources_of_notice)
    def format_source(source, referenced_in_notices):
        result = html.format_lexicon_entry(source, referenced_in_notices, True, False)
        all_references = records[heurist.Reference.RECORD_TYPE]
        references_hid = getattr(source, FIELD_KEY, [])
        if len(references_hid) > 0:
            references = [all_references[hid] for hid in references_hid if all_references[hid].reference]
            references_vs_notices = dict()
            keys_vs_references = dict()
            for reference in references:
                in_notices = getattr(reference, IN_NOTICES_KEY, [])
                if len(in_notices) < 1:
                    continue
                # special case: more than 1 notices point on the same reference
                # of the same source BUT this reference is duplicated in the DB
                def find_duplicate_if_any(ref, d):
                    k = ' '.join(ref.reference)
                    for key, r in d.items():
                        if key == k:
                            return k, r  # r is a duplicate of ref
                    return k, ref  # ref is not a duplicate, at least for now
                key, ref = find_duplicate_if_any(reference, keys_vs_references)
                previous = references_vs_notices.get(ref, [])
                in_notices = previous + in_notices
                in_notices.sort()
                references_vs_notices[ref] = in_notices
                keys_vs_references[key] = ref
            from functools import cmp_to_key
            from html import compare_references
            references = list(references_vs_notices.keys())
            references.sort(key=cmp_to_key(compare_references))
            for reference in references:
                in_notices = references_vs_notices[reference]
                result += html.get_word_and_refs(reference, in_notices)
        elif len(references_hid) < 1:
            # if neither source nor any of its references is cited
            # in a Notice, do not publish it
            return None
        return result + '</section>'
    sources.sort(key=lambda x: htmlz.strip_diacritics(x.descripteur))
    sources_html = lexicon_as_html(sources, entry_formatter=format_source)

    # WRITE HTML
    periodiques = []
    for revue in revues:
        periodiques.append(revue.as_entry())
    ouvrages_collectifs = []
    for oc in ocs:
        ouvrages_collectifs.append(oc.as_entry(records))
    corpus = []
    for source in sources:
        if source.alias and not source.is_author:
            corpus.append(source.as_entry())

    articles = []

    def get_new_initial_html(old, new):
        if old == new:
            return None
        prefix = "</section>" if old else ""
        title = ("<section><h3 class='header hide'>%s</h3>" % new) if new else ""
        return '%s%s' % (prefix, title)
    initial = None
    for notice in notices:
        authors_str = notice.get_authors_string(records)
        new_initial = authors_str[0][0] if authors_str else None
        initial_str = get_new_initial_html(initial, new_initial)
        if initial_str:
            articles.append(initial_str)
        initial = new_initial
        articles.append(notice.as_html(records))
    articles.append(get_new_initial_html(initial, None))

    volume = config.number
    millesime = '20%s' % config.year
    path = 'resources/chapters/%s/foreword.html' % millesime
    from os.path import isfile
    if not isfile(path):
        path = 'resources/chapters/default/foreword.html'
    with open(path, 'r') as f:
        foreword = f.read()
        foreword = foreword.format(
                volume=volume, millesime=millesime,
                notices=len(notices), revues=len(revues), ocs=len(ocs),
                )
    path = 'resources/chapters/%s/collections.html' % millesime
    from os.path import isfile
    if not isfile(path):
        path = 'resources/chapters/default/collections.html'
    with open(path, 'r') as f:
        dans_la_meme_collection = f.read()
        dans_la_meme_collection = dans_la_meme_collection.format(
                volume=volume, millesime=millesime,
                )

    with open(config.style, 'r') as f:
        style = f.read()

    with open(config.template, 'r') as f:
        template = f.read()

    with open(config.output, 'a') as fd:
        fd.write(template.format(
            style=style,
            volume=volume, millesime=millesime,
            contributeurs = ', '.join(contributeurs),
            foreword=foreword,
            langues=languages,
            revues='\n'.join(periodiques),
            ouvrages_collectifs='\n'.join(ouvrages_collectifs),
            corpus='\n'.join(corpus),
            notices='\n'.join(articles),
            authors='\n'.join(authors_html),
            anthros='\n'.join(anthros_html),
            sources='\n'.join(sources_html),
            places='\n'.join(places_html),
            themes='\n'.join(themes_html),
            collection=dans_la_meme_collection,
            ))

    if config.verbose:
        output_elements(records)

    if len(errors) > 0:
        raise Exception('\n'.join(errors))


def output_elements(records):
    for record_id, records_map in records.items():
        if record_id == '1':
            record_type = 'Relation'
        elif record_id == '71':
            record_type = 'Période'
        else:
            first = records_map[list(records_map.keys())[0]]
            record_type = type(first).__name__
        number = len(records_map)
        print('%3s:%s: %s éléments' % (record_id, record_type, number))


def parse(config):
    tree = parse_tree(config.input)
    events = ('start', 'end')
    context = etree.iterwalk(tree.getroot(), events=events)
    records = unmarshall(context)
    try:
        write_html(config, records)
    except Exception:
        import traceback
        traceback.print_exc()


if __name__ == '__main__':
    config = create_parser().parse_args()
    parse(config)
