# -*- coding: utf-8 -*-
import argparse
from os.path import dirname, realpath, join

def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('input',
            help="Input file path (Heurist XML / HML)")
    parser.add_argument('-o', '--output', default='output.html',
            help="Output file path")
    parser.add_argument('-t', '--template', default=default_template_path(),
            help="Template file path")
    parser.add_argument('-l', '--language_codes', action='store_true',
            help="Languages vocabulary CSV file path")
    parser.add_argument('-s', '--style', default=default_style_path(),
            help="Stylesheet file path")
    parser.add_argument('-n', '--number',
            help="Volume number")
    parser.add_argument('-y', '--year',
            help="Publishing year, on two digits (20xx is assumed)")
    parser.add_argument('--thesaurus', action='store_true',
            help="If present, will output thesaurus file instead of book.")
    parser.add_argument('-v', '--verbose', action='store_true',
            help="Verbose mode")
    return parser

def default_template_path():
    return join(dirname(realpath(__file__)), 'template.html')

def default_style_path():
    return join(dirname(realpath(__file__)), 'candy.css')
