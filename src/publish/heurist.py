# -*- coding: utf-8 -*-

# 68: Notice, 1860
# 58: Auteur, 2154
# 69: Signature, 10
# 65: Mot-clef, 3512
# 61: Lieu, 1533
# 59: Anthroponyme, 1330
# 54: Revue, 160
# 72: Référence, 4206
# 66: Ouvrage Collectif, 16
# 62: Peuple, 149
# 71: Période, 1
# 60: Source, 721
# 67: Collection, 12
# 1: Record Relationship, 3815

class Record(object):
    def __init__(self, fields):
        self.fields = fields

    @property
    def hid(self):
        return self.fields['hid']

    @property
    def suffix(self):
        return type(self).__name__.lower()

    @property
    def html_id(self):
        return self.suffix + '_' + self.hid

    def get(self, key, default=None):
        return self.fields.get(self.FIELDS[key], default)

    def set(self, key, value):
        self.fields[self.FIELDS[key]] = value

    def validate(self, records, errors):
        pass


class Notice(Record):
    RECORD_TYPE = '68'
    FIELDS = {
        'titre': '2-1',
        'auteurs': '0-954',
        'signatures': '0-981',
        'etat': '0-968',
        'origine': '0-964',
        'millesime': '0-960',
        'numero': '0-961',
        'fascicule': '0000-986',
        'pages': '0-962',
        'langues_article': '0-970',
        'langues_resume': '0-971',
        'chronologie_debut': '0-972',
        'chronologie_fin': '0-973',
        'anthroponymes': '0-975',
        'noms_geographiques': '0-976',
        'peuples': '0-977',
        'sources': '0-978',
        'mots_clefs': '0-979',
        'notes': '0-980',
        }

    LANGUAGES = {
        'Albanais': ['ALB', False],
        'Allemand': ['GER', False],
        'Anglais': ['ENG', False],
        'Arabe': ['ARA', False],
        'Bulgare': ['BUL', False],
        'Catalan': ['CAT', False],
        'Croate': ['CRO', False],
        'Espagnol': ['SPA', False],
        'Finlandais': ['FIN', False],
        'Français': ['FRE', False],
        'Grec': ['GRE', False],
        'Hebreu': ['HEB', False],
        'Hongrois': ['HUN', False],
        'Italien': ['ITA', False],
        'Latin': ['LAT', False],
        'Macédonien': ['MKD', False],
        'Polonais': ['POL', False],
        'Portugais': ['POR', False],
        'Roumain': ['RUM', False],
        'Russe': ['RUS', False],
        'Serbe': ['SER', False],
        'Slovaque': ['SLK', False],
        'Turc': ['TUR', False],
        'Turque': ['TUR', False],  # retro-compatibility with old exports
        'Ukrainien': ['UKR', False],
        }

    def validate(self, records, errors):
        titres = self.get('titre')
        if len(titres) < 1:
            errors.append("Notice '%s': Titre manquant." % self.hid)
        elif len(titres) > 1:
            errors.append("Notice '%s': Trop de titres: %s." % (self.hid, titres))
        self.set('titre', titres[0])

        authors = records[Auteur.RECORD_TYPE]
        for hid in self.get('auteurs', []):
            if not authors.get(hid, None):
                errors.append("Notice '%s': Auteur '%s' inconnu." % (self.hid, hid))

        origines = self.get('origine')
        if not origines:
            errors.append("Notice '%s': Revue ou OC manquant." % (self.hid))
        if len(origines) > 1:
            errors.append("Notice '%s': Trop de revues/oc: %s." % (self.hid, origines))
        for hid in origines:
            if not self.get_origin(records, hid):
                errors.append("Notice '%s': revue/oc '%s' inconnu." % (self.hid, hid))

        languages = self.get('langues_article', [])
        for index, language in enumerate(languages):
            languages[index] = self.set_language_code(language)
        languages = self.get('langues_resume', [])
        for index, language in enumerate(languages):
            languages[index] = self.set_language_code(language)

    def set_language_code(self, language):
        try:
            code = self.LANGUAGES[language][0]
            self.LANGUAGES[language][1] = True
            return code
        except KeyError as ex:
            print("ERROR: Missing language code '%s'" % language)
            raise ex

    def get_origin(self, records, hid):
        try:
            revues = records[Revue.RECORD_TYPE]
            return revues[hid]
        except KeyError:
            try:
                ouvrages = records[OuvrageCollectif.RECORD_TYPE]
                return ouvrages[hid]
            except KeyError:
                return None


class Auteur(Record):
    RECORD_TYPE = '58'
    FIELDS = {
        'nom': '2-1',
        'prenom': '2-18',
        }

    @property
    def nom(self):
        return self.fields[self.FIELDS['nom']]

    @property
    def initial(self):
        return self.nom[0]

    @property
    def et_al(self):
        return self.nom.lower().startswith('et al')

    def validate(self, records, errors):
        noms = self.get('nom')
        if type(noms) is list:
            self.set('nom', ' '.join(noms))
        prenoms = self.get('prenom')
        if type(prenoms) is list:
            self.set('prenom', ' '.join(prenoms))

    def __repr__(self):
        if self.et_al:
            return self.nom
        prenom = self.get('prenom')
        prenom = ' (%s)' % prenom if prenom else ''
        return '%s%s' % (self.nom, prenom)

    def __lt__(self, other):
        return str(self) < str(other)


class Signature(Auteur):
    RECORD_TYPE = '69'


class Revue(Record):
    RECORD_TYPE = '54'
    FIELDS = {
        'titre': '2-1',
        'lieu': '1125-129',
        'abbreviation': '0-941',
        'ISSN': '0-943',
        }

    def validate(self, records, errors):
        pass


class OuvrageCollectif(Record):
    RECORD_TYPE = '66'
    FIELDS = {
        'titre': '2-1',
        'collection': '963',
        'auteurs': '0-954',
        'millesime': '0-960',
        'numéro': '0-961',
        'pages': '0-962',
        'notes': '0-980',
        }

    def validate(self, records, errors):
        pass

    @property
    def numero(self):
        numero = self.get('numéro')
        if numero:
            return numero[0]
        return None

    def get_collection(self, records):
        hid = self.get('collection')
        if not hid:
            return None
        hid = hid[0]
        try:
            collections = records[Collection.RECORD_TYPE]
            return collections[hid]
        except KeyError:
            return None



class Collection(Record):
    RECORD_TYPE = '67'
    FIELDS = {
        'titre': '2-1',
        'lieu': '1125-129',
        'abbreviation': '0-941',
        }

    def validate(self, records, errors):
        pass


class Tag(Record):
    FIELDS = {
        'descripteur': '2-1',
        }

    @property
    def descripteur(self):
        return self.fields[self.FIELDS['descripteur']][0]

    @property
    def initial(self):
        return self.descripteur[0]

    def __repr__(self):
        return self.descripteur.upper()


class Anthroponyme(Tag):
    RECORD_TYPE = '59'


class Lieu(Tag):
    RECORD_TYPE = '61'


class Peuple(Tag):
    RECORD_TYPE = '62'


class Source(Record):
    RECORD_TYPE = '60'
    FIELDS = {
        'descripteur': '2-1',
        'alias': '3-1009',
        'is_author': '0-988',
        }

    @property
    def descripteur(self):
        return self.fields[self.FIELDS['descripteur']][0]

    @property
    def alias(self):
        value = self.fields.get(self.FIELDS['alias'], None)
        return value[0] if value else None

    @property
    def is_author(self):
        value = self.fields.get(self.FIELDS['is_author'], False)
        return True if value and value[0] == "Oui" else False

    @property
    def initial(self):
        return self.descripteur[0]

    def validate(self, records, errors):
        pass  # TODO verify relations existence ?

    def __repr__(self):
        return self.descripteur


class Reference(Record):
    RECORD_TYPE = '72'
    FIELDS = {
        'reference': '2-1',
        'source': '0-967',
        }

    def validate(self, records, errors):
        sources = records[Source.RECORD_TYPE]
        l = self.get('source', [])
        if l and len(l) > 1:
            errors.append("Notice '%s': Plus d'une source: %s." % (self.hid, l))
        for hid in l:
            source = self.get_source(records, hid)
            if not source:
                errors.append("Notice '%s': Source '%s' inconnue." % (self.hid, hid))
        # Replace 'reference' field, which is a list of only one element,
        # which, if present, is a spaces-separated strings,
        # by just a list of strings.
        # We COULD convert all members of this new list to be integers,
        # however these can be things like 'EA', '909C' or '279-290' too ...
        # so we leave the problem to html.compare_references .
        ref = self.get('reference', None)
        if ref:
            ref = ref[0].split(' ')
        self.set('reference', ref)

    @property
    def source(self):
        sources = self.fields.get(self.FIELDS['source'], [])
        if sources:
            return sources[0]
        return None

    @property
    def reference(self):
        return self.get('reference', None)

    @staticmethod
    def get_source(records, hid):
        sources = records[Source.RECORD_TYPE]
        return sources.get(hid, None)

    def __repr__(self):
        return ' '.join(self.reference) if self.reference else ""
