# -*- coding: utf-8 -*-
import heurist
from htmlz import list2html, Auteur, Keyword, Anthroponyme, Lieu, Peuple


class Signature(heurist.Signature):
    def as_html(self, records):
        noms = self.fields[self.FIELDS['nom']]
        prenoms = self.fields[self.FIELDS['prenom']]
        # TODO missing: CNRS, Unistra, ...
        return '%s %s' % (prenoms, noms)


class Collection(heurist.Collection):
    pass

class Source(heurist.Source):
    def as_html(self, records):
        return '%s (%s)' % (self.get('descripteur'), self.get('alias'))

    def as_entry(self):
        '''Formattage dans la liste des corpus et recueils de sources.

        Renvoie la ligne correspondante à cette Source dans le chapitre
        référençant tous les corpus et sources présents dans ce volume.
        Ce formattage n'est utilisé qu'une seule fois, en début d'ouvrage.

            Returns:
                Un élément <table /> d'uid self.html_id.
                Cet élément n'a qu'une seule <tr /> afin de pouvoir gérer des
                sauts de page au sein de ce chapitre (entre deux tables).
        '''
        return '''<table><tr id='{uid}'>
<td>{abbr}</td>
<td><em>{titre}</em></td></tr></table>'''.format(
                uid=self.html_id,
                abbr=self.descripteur.strip(),
                titre=self.alias.strip() if self.alias else "?",
                )


class Reference(heurist.Reference):
    def as_html(self, records):
        source = self.get_source(records)
        return '%s %s' % (source.get('descripteur'), self.get('reference'))


def compare_references(x, y):
    x = x.reference
    y = y.reference
    if not x:
        return -1
    if not y:
        return 1
    size = min(len(x), len(y))
    for i in range(0, size):
        try:
            res = int(x[i]) - int(y[i])
            if res == 0:
                continue
            return res
        except ValueError:
            sx = str(x[i])
            sy = str(y[i])
            from itertools import takewhile
            dx = ''.join(takewhile(str.isdigit, sx))
            dy = ''.join(takewhile(str.isdigit, sy))
            if dx and dy:  # stuff like '909C' or '279-290'
                try:
                    res = int(dx) - int(dy)
                    if res != 0:
                        return res
                except ValueError:
                    pass  # probably something like '1²'
            # stuff like 'EA'
            if sx == sy:
                continue
            if sx < sy:
                return -1
            if sx > sy:
                return 1
    return len(x) - len(y)  # '1 41' > '1'


def list2str(records, identifiers, target, prefix='', formatter=None):
    if not formatter:
        formatter = lambda t, records: t.as_html(records)
    parts = []
    if identifiers:
        targets = records[target.RECORD_TYPE]
        for hid in identifiers:
            parts.append(formatter(targets[hid], records))
    result = ''
    if parts:
        result = '%s%s' % (prefix, ', '.join(parts))
    return result


def get_word_and_refs(e, references, level=6):
    refs = ['<a href=\'#notice-%s\'>%s</a>' % (r, r) for r in references]
    return '''<div>
                <h{l} class='word' id='{id_}'>{word}</h{l}>
                <span class='refs'>{refs}</span>
              </div>'''.format(
                      l=level, id_=e.html_id, word=str(e),
                      refs=', '.join(refs))

def format_lexicon_entry(e, references, allow_empty_list=False, close=True):
    if len(references) < 1 and not allow_empty_list:
        return None
    str_ = get_word_and_refs(e, references, level=5)
    suffix = '''</section>''' if close else ''
    return '''<section>%s%s''' % (str_, suffix)
