# -*- coding: utf-8 -*-
import anytree
import heurist


class Keyword(heurist.Tag, anytree.NodeMixin):
    RECORD_TYPE = '65'
    FIELDS = {
        'descripteur': '2-1',
        'generique': '6260',
        'specifiques': '6261',
        'voir_aussi': '6262',
        'utilise_pour': '6263',
        'synonyme': '6259',
        'candidat': '0-983',
        }
    is_synonyme = False

    @property
    def candidat(self):
        try:
            return self.fields[self.FIELDS['candidat']][0] == 'Oui'
        except KeyError:
            return False

    def validate(self, records, errors):
        keywords = records[self.RECORD_TYPE]
        parents = self.get('generique')
        if parents:
            if len(parents) > 1:
                errors.append("Keyword '%s': too many parents (%s) %s."
                              % (self.hid, len(parents), parents))
        children = self.get('specifiques', [])
        for hid in children:
            try:
                keywords[hid]
            except KeyError:
                errors.append("Keyword '%s': child '%s' not found."
                              % (self.hid, hid))

    def update(self, keywords):
        parent = self.get('generique', None)
        if parent:
            self.parent = keywords[parent[0]]
        specifiques = self.get('specifiques', [])
        for hid in specifiques:
            try:
                child = keywords[hid]
                child.parent = self
                # no need to self.children.append(child),
                # anytree takes care of it
            except KeyError:
                print("Keyword '%s': child keyword '%s' not found."
                      % (self.hid, hid))
        synonymes = self.get('synonyme', [])
        for hid in synonymes:
            synonyme = keywords[hid]
            if synonyme == self:
                continue
            synonyme.is_synonyme = True
            synonyme.parent = self
        synonymes = self.get('utilise_pour', [])
        for hid in synonymes:
            synonyme = keywords[hid]
            if synonyme == self:
                continue
            self.is_synonyme = True
            self.parent = synonyme
        '''
        synonymes = self.get('voir_aussi', [])
        for hid in synonymes:
            synonyme = keywords[hid]
            if synonyme == self:
                continue
            self.is_synonyme = True
            self.parent = synonyme
        '''



def update_tree(keywords):
    for key, keyword in keywords.items():
        keyword.update(keywords)
    roots = []
    candidates = []
    orphans = []
    for key, keyword in keywords.items():
        if keyword.candidat:
            candidates.append(keyword)
        elif not keyword.parent:
            if keyword.children:
                roots.append(keyword)
            else:
                orphans.append(keyword)
    return roots, candidates, orphans



def output_tree(fd, roots, candidates, orphans):
    def kwsort(items):
        return sorted(items, key=lambda kw: kw.descripteur)

    # TODO improve this hard-and-fast txt output
    def myrender(kw):
        pre = '[s] ' if getattr(kw, 'is_synonyme', False) else ''
        return pre + kw.descripteur
    for root in roots:
        for pre, fill, node in anytree.RenderTree(root, childiter=kwsort):
            print("%s%s" % (pre, myrender(node)))
    # TODO end

    fd.write('''<html>
<body>
<style>
/* fold/unfold logic */
.unfolder ~ .fold { display: none; }
.unfolder:checked ~ .fold { display: block; }
/* arrow logic */
ul { list-style-type: none; }
span[class$="icon"] { color: #999; margin-right: 0.25em; }
.unfolder ~ label .fold-icon { display: none; }
.unfolder:checked ~ label .fold-icon { display: inline-block; }
.unfolder:checked ~ label .unfold-icon { display: none; }

.synonym { color: #999; }
</style>
''')
    r = Renderer(fd)
    for root in roots:
        walk(node=root, start=r.start, end=r.end, iter_children=kwsort)
    fd.write('''
</body>
</html>''')

def walk(node, start, end, iter_children, level=0):
    start(node, level)
    for child in iter_children(node.children):
        walk(child, start, end, iter_children, level+1)
    end(node, level)

class Renderer(object):

    def __init__(self, fd):
        self.fd = fd
        self.nodes = 0

    def start(self, kw, level=0):
        self.nodes += 1
        self.fd.write('''<ul class='fold'>\n''')
        icon = '&#9642;'
        class_ = 'standard'
        if kw.is_synonyme:
            icon = '&#10697;'
            class_ = 'synonym'
        if kw.is_leaf:
            self.fd.write('''<li>
  <span class='leaf-icon'>%s</span>
  <span class='%s'>%s</span>\n''' % (icon, class_, kw.descripteur))
        else:
            self.fd.write('''<li>
  <input id='toggle%s' type='checkbox' class='unfolder' style='display: none;' />
  <label for='toggle%s' style='display: inline-block; cursor: pointer;'>
    <span class='unfold-icon'>&#9654;</span>
    <span class='fold-icon'>&#9660;</span>
    <span class='%s'>%s</span>
  </label>
  ''' % (self.nodes, self.nodes, class_, kw.descripteur))

    def end(self, kw, level=0):
        self.fd.write('''</li></ul>''')
