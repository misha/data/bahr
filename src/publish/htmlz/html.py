# -*- coding: utf-8 -*-


def get(records, identifiers, target):
    targets = records[target.RECORD_TYPE]
    return get_from_targets(targets, identifiers)


def get_from_targets(targets, identifiers):
    if identifiers:
        return [targets[hid] for hid in identifiers]
    return []


def get_records(records, identifiers, target):
    results = []
    for record in get(records, identifiers, target):
        results.append(record)
    return results


def list2html(records, identifiers, target, prefix=''):
    l = get_records(records, identifiers, target)
    parts = []
    for record in l:
        parts.append(record.html)
    html = ''
    if parts:
        html = '%s%s' % (prefix, ', '.join(parts))
    return html


def strip_diacritics(s):
    '''Nettoie les dïàçriTiqµ€$ de la chaîne de caractères en entrée.

    Cela permet par exemple de s'assurer que ces caractères diacritiques
    ne perturberont pas un tri alphabétique ultérieur.

        Returns:
            dïàçriTiqµ€$ --> diacriTiques
    '''
    import unicodedata
    return (''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')).replace('Ć', 'C').replace('ć', 'c').replace('Đ', 'D').replace('đ', 'd').replace('Ł', 'L').replace('ł', 'l').replace('Ĺ', 'L').replace('ĺ', 'l').replace('Ñ', 'N').replace('ñ', 'n')
