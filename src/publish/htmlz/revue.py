# -*- coding: utf-8 -*-
from heurist import Revue as HeuristObject


class Revue(HeuristObject):
    @property
    def html(self):
        '''Formattage dans la description de chaque notice.

            Returns:
                Un élément <a /> pointant vers self.html_id
        '''
        return '''<a href='#{uid}'><em>{abbr}</em></a>'''.format(
                uid=self.html_id,
                abbr=self.get('abbreviation')[0]
                )

    @property
    def lieu(self):
        lieu = self.get('lieu')
        return lieu[0].strip() if lieu else None

    def as_entry(self):
        '''Formattage dans la liste des périodiques.

        Renvoie la ligne correspondante à cette Revue dans le chapitre
        référençant tous les périodiques présents dans ce volume.
        Ce formattage n'est utilisé qu'une seule fois, en début d'ouvrage.

            Returns:
                Un élément <table /> d'uid self.html_id.
                Cet élément n'a qu'une seule <tr /> afin de pouvoir gérer des
                sauts de page au sein de ce chapitre (entre deux tables).
        '''
        return '''<table><tr id='{uid}'>
<td><em>{abbr}</em></td>
<td><em>{titre}</em>{lieu}</td></tr></table>'''.format(
                uid=self.html_id,
                abbr=self.get('abbreviation')[0].strip(),
                titre=self.get('titre')[0].strip(),
                lieu=" – %s" % self.lieu if self.lieu else "",
                )
