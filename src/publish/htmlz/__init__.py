# -*- coding: utf-8 -*-

from .notice import *
from .auteur import *
from .revue import *
from .oc import *
from .keywords import *
from .html import get, list2html, strip_diacritics
