# -*- coding: utf-8 -*-
from heurist import OuvrageCollectif as HeuristObject
from .html import get
from .auteur import Auteur


class OuvrageCollectif(HeuristObject):

    def as_html(self, records):
        collection = self.get_collection_for_notice(records)
        return '''in: <em><a href='#{uid}'>{titre}</a></em>, <span class='authors'>{auteurs}</span>{collection}, {annee}'''.format(
                uid=self.html_id,
                titre=self.get('titre')[0],
                auteurs=self.get_authors_str(records, True),
                collection=collection,
                annee=self.get('millesime')[0],
                )

    def as_entry(self, records):
        '''Formattage dans la liste des ouvrages collectifs.

        Renvoie la ligne correspondante à cette Revue dans le chapitre
        référencant tous les ouvrages collectifs présents dans ce volume.
        Ce formattage n'est utilisé qu'une seule fois, en début d'ouvrage.

            Returns:
                Un élément <p /> d'uid self.html_id
        '''
        notes = self.get('notes', None)
        notes = ", %s" % notes[0] if notes else ""
        auteurs = self.get_authors_str(records, False)
        collection = self.get_collection_str(records)
        return '''<p id='{uid}'>
<span class='authors'>{auteurs}</span> (éd.), <em>{titre}</em>{notes}{collection}, {annee}
</p>'''.format(
                uid=self.html_id,
                titre=self.get('titre')[0],
                notes=notes,
                auteurs=auteurs,
                collection=collection,
                annee=self.get('millesime')[0],
                )

    def get_authors(self, records):
        '''Accès aux auteurs de cet ouvrage.

            Returns:
                Une liste d'objets Auteur
        '''
        return get(records, self.get('auteurs'), Auteur)

    def get_authors_str(self, records, ed_after_each=True):
        '''Formattage de la liste des auteurs de cet ouvrage.

            Parameters:
                records (map): Tous les records de ce volume
                ed_after_each (boolean): False depuis self.as_entry, True sinon

            Returns:
                Du texte HTML
        '''
        formatter = lambda t: t.html
        if ed_after_each:
            formatter = lambda t: '%s (éd.)' % t.html
        authors = self.get_authors(records)
        return ', '.join(formatter(author) for author in authors)

    def get_collection_str(self, records):
        collection = self.get_collection(records)
        if not collection:
            return ""
        abreviation = collection.get('abbreviation', None)
        abreviation = "%s – " % abreviation[0] if abreviation else ""
        titre = collection.get('titre')[0]
        lieu = collection.get('lieu', None)
        lieu = ", %s" % lieu[0] if lieu else ""
        numero = ", %s" % self.numero if self.numero else ""
        return ' (%s%s%s%s)' % (abreviation, titre, lieu, numero)

    def get_collection_for_notice(self, records):
        collection = self.get_collection(records)
        if not collection:
            return ""
        abreviation = collection.get('abbreviation', None)
        abreviation = ", %s" % abreviation[0] if abreviation else ""
        numero = ", %s" % self.numero if self.numero else ""
        return '%s%s' % (abreviation, numero)
