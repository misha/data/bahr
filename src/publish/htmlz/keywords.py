# -*- coding: utf-8 -*-
import heurist
import thesaurus


class Keyword(thesaurus.Keyword):
    @property
    def html(self):
        '''Formattage dans la description de chaque notice.

            Returns:
                Un élément <a /> pointant vers self.html_id
        '''
        return '<a href=#%s>%s</a>' % (self.html_id, repr(self))


class Anthroponyme(heurist.Anthroponyme):
    @property
    def html(self):
        '''Formattage dans la description de chaque notice.

            Returns:
                Un élément <a /> pointant vers self.html_id
        '''
        return '<a href=#%s>%s</a>' % (self.html_id, repr(self))


class Lieu(heurist.Lieu):
    @property
    def html(self):
        '''Formattage dans la description de chaque notice.

            Returns:
                Un élément <a /> pointant vers self.html_id
        '''
        return '<a href=#%s>%s</a>' % (self.html_id, repr(self))


class Peuple(heurist.Peuple):
    @property
    def html(self):
        '''Formattage dans la description de chaque notice.

            Returns:
                Un élément <a /> pointant vers self.html_id
        '''
        return '<a href=#%s>%s</a>' % (self.html_id, repr(self))


