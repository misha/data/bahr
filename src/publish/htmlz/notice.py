# -*- coding: utf-8 -*-
import heurist
from .auteur import Auteur
from .html import get_records, list2html
from .keywords import Peuple, Anthroponyme, Lieu, Keyword


class Notice(heurist.Notice):
    def as_html(self, records):
        chars = 0
        chronology = self.get_chronology_string()
        chars, prefix = update_area_separator(chars, chronology)
        peuples = self.list2html(records, 'peuples', Peuple, prefix)
        chars, prefix = update_area_separator(chars, peuples)
        anthros = self.list2html(records, 'anthroponymes', Anthroponyme, prefix)
        chars, prefix = update_area_separator(chars, anthros)
        lieux = self.list2html(records, 'noms_geographiques', Lieu, prefix)
        chars, prefix = update_area_separator(chars, lieux)
        sources = self.get_sources_string(records, prefix)
        chars, prefix = update_area_separator(chars, sources)
        keywords = self.list2html(records, 'mots_clefs', Keyword, prefix)
        return '''
<article class='notice'>
<div>
<span class='reference' id='notice-{bahr_book_ref}'>{bahr_book_ref}</span>
<span class='authors'>{auteurs}</span>{auteurs_sep}{titre}, {origine}{langues}
</div>{notes}
<div class='keywords'>
{chronology}{peuples}{anthros}{lieux}{sources}{keywords}
</div>
</article>'''.format(
        bahr_book_ref=self.bahr_book_ref,
        titre=self.get_title_string(),
        origine=self.get_origin_string(records),
        auteurs=self.list2html(records, 'auteurs', Auteur),
        auteurs_sep=(', ' if self.get('auteurs') else ''),
        langues=self.get_languages_string(),
        notes=self.get_notes_string(),
        chronology=chronology,
        sources=sources,
        peuples=peuples,
        anthros=anthros,
        lieux=lieux,
        keywords=keywords,
        )

    def list2html(self, records, key, target, prefix=''):
        if not hasattr(self, key):
            html = list2html(records, self.get(key), target, prefix)
            setattr(self, key, html)
        return getattr(self, key)

    def get_title_string(self):
        titre = self.get('titre')
        titre = titre.strip()
        titre = titre.replace('\\"', '"')
        titre = titre.replace('\\\'', '\'')
        while titre.endswith('.'):
            titre = titre[:-1]
        return titre

    def get_sorting_string(self, records):
        '''Renvoie la liste d'éléments utilisée pour le tri.

        Si le premier élémént de la liste est le même, on passe au second, etc.
        Classique Python, quoi : tout ça est prévu pour matcher avec sort().

        L'élément principal sur lequel le tri est effectué, c'est les auteurs,
        triés par ordre dans la publication (voir fonction get_authors_string).
        Cependant, si exactement les mêmes auteurs ont publié cette année du
        BAHR, il faut ensuite trier par nom de publication, puis par année, etc
        (voir fonction get_origin_string). Par acquis de conscience, j'ai aussi
        rajouté un tri par titre de l'article tout à la fin.
        '''
        parts = self.get_authors_string(records)
        parts.append(self.get_origin_string(records))
        parts.append(self.get_title_string())
        return parts

    def get_authors_string(self, records):
        '''Renvoie la liste des auteurs de la notice.

        La liste renvoyée est une liste de chaînes de caractères représentant
        chacune un auteur. Chacune de ces chaînes de caractères est débarrassée
        des caractères diacritiques (accents, ...).
        Le résultat de cette fonction N'EST donc PAS à être publié tel quel ;
        au contraire, cette fonction n'existe que pour faciliter un tri
        par ordre lexicographique ultérieur (ordonnancement des notices).
        Au sein de la liste, les auteurs ne sont eux-mêmes pas non plus triés
        alphabétiquement, pour préserver l'ordre originel de la publication.
        '''
        from htmlz import strip_diacritics
        authors = get_records(records, self.get('auteurs'), Auteur)
        return [strip_diacritics(str(a).replace('-', '')) for a in authors]

    def get_origin_string(self, records):
        hid = self.get('origine')[0]
        origin = self.get_origin(records, hid)
        page = self.get_pages_string()
        tome = self.get('numero', None)
        tome = ', %s' % tome[0] if tome else ''
        fasc = self.get('fascicule', None)
        fasc = ', %s' % '-'.join(fasc) if fasc else ''
        year = self.get('millesime', None)
        year = ', %s' % year[0] if year else ''
        try:  # Revue
            origin_html = origin.html
        except AttributeError:  # OuvrageCollectif
            origin_html = origin.as_html(records)
        return '%s%s%s%s%s' % (origin_html, tome, fasc, year, page)

    def get_pages_string(self):
        pages = self.get('pages')
        if not pages:
            return ''
        l = []
        for p in pages:
            # shield against some pages syntax inconsistencies in database
            # "27-28", "p. 27-28", "en ligne", "revue en ligne" and so on ...
            p = p.lstrip()
            if p.startswith('p.'):
                p = p[len('p.'):].strip()
            if not p:
                continue
            if p[0].isdigit():
                p = 'p.\xa0%s' % p  # \xa0 is non-breaking space
            l.append(p)
        if not l:
            return ''
        return ', %s' % ', '.join(l)

    def update_sources(self, records):
        key = 'references'
        if not hasattr(self, key):
            references = self.get('sources')
            if not references:
                setattr(self, key, dict())
                return ''
            all_refs = records[heurist.Reference.RECORD_TYPE]
            refs = dict()
            for hid in references:
                reference = None
                try:
                    reference = all_refs[hid]
                    if not reference.get('source'):
                        continue
                    source_hid = reference.get('source')[0]
                except KeyError:
                    source_hid = hid
                source = heurist.Reference.get_source(records, source_hid)
                l = refs.get(source, [])
                if reference:
                    ref = reference.get('reference')
                    if ref:
                        l.append(' '.join(ref))
                refs[source] = l
            setattr(self, key, refs)
        return key

    def get_sources_string(self, records, prefix=''):
        key = self.update_sources(records)
        result = ''
        for source, refs in getattr(self, key, {}).items():
            abbrev = source.get('descripteur')[0]
            result += ' %s %s %s' % (prefix, abbrev, ' ; '.join(refs))
        return result

    def get_languages_string(self):
        langues_article = self.get('langues_article')
        langues_resume = self.get('langues_resume')
        if not langues_article and not langues_resume:
            return ''
        result = ', '
        if langues_article:
            result += ', '.join(langues_article)
            result += ' '
        if langues_resume:
            result += '(rés. %s)' % ', '.join(langues_resume)
        return result

    def get_notes_string(self):
        notes = self.get('notes')
        return '\n<div>(%s)</div>' % ' '.join(notes) if notes else ''

    def get_chronology_string(self):
        start = self.get('chronologie_debut')
        end = self.get('chronologie_fin')
        result = ''
        if start:
            result += format_chronology(start[0])
        if end:
            if start:
                result += ' – '  # https://en.wikipedia.org/wiki/Thin_space
            result += format_chronology(end[0])
        return result

def format_chronology(date):
    date = date.replace(' ', '')
    if date[0] == '-':
        return date
    return '+%s' % date

def update_area_separator(chars, s):
    chars += len(s)
    if chars == 0:
        return chars, ''
    return chars, ' – '  # https://en.wikipedia.org/wiki/Thin_space
