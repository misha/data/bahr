# -*- coding: utf-8 -*-
from heurist import Auteur as HeuristObject


class Auteur(HeuristObject):
    @property
    def html(self):
        '''Formattage dans la description de chaque notice.

            Returns:
                Un élément <a /> pointant vers self.html_id
        '''
        return '<a href=#%s>%s</a>' % (self.html_id, repr(self))
