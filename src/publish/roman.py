# -*- coding: utf-8 -*-
'''
Credit: Paul M. Wrinkler
https://www.oreilly.com/library/view/python-cookbook/0596001673/ch03s24.html
'''

ROMAN_DIGITS = {
        'M': 1000,
        'D': 500,
        'C': 100,
        'L': 50,
        'X': 10,
        'V': 5,
        'I': 1,
        }


def int2roman(value):
    if not isinstance(value, type(1)):
        raise TypeError("Expected integer, got %s" % type(value))
    if not 0 < value < 4000:
        raise ValueError("Argument must be between 1 and 3999")
    ints = (1000, 900,  500, 400, 100,  90,
            50, 40, 10, 9, 5, 4, 1)
    nums = ('M', 'CM', 'D', 'CD', 'C', 'XC',
            'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
    result = []
    for i in range(len(ints)):
        count = int(value / ints[i])
        result.append(nums[i] * count)
        value -= ints[i] * count
    return ''.join(result)


def roman2int(roman):
    if not isinstance(roman, str):
        raise TypeError("Expected string, got %s" % type(roman))
    roman = roman.upper()
    count = 0
    for i in range(len(roman)):
        try:
            value = ROMAN_DIGITS[roman[i]]
            # If the next place holds a larger number, this value is negative
            if i+1 < len(roman) and ROMAN_DIGITS[roman[i+1]] > value:
                count -= value
            else:
                count += value
        except KeyError:
            raise ValueError("'%s' is not a valid Roman numeral" % roman)
    reverse = int2roman(count)
    if reverse == roman:
        return count
    else:
        msg = "'%s' is not a valid Roman numeral (%s)" % (roman, reverse)
        raise ValueError(msg)
