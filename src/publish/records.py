# -*- coding: utf-8 -*-


class Record(object):
    records = {}  # map: record type <> records map
    TAG = 'record'
    inside = False
    datatype = None  # record type number (can be seen in Heurist)
    attrs = None  # map: field_id <> field value

    def start(self, e):
        tag = Record.get_tag(e)
        if tag == self.TAG:
            self.inside = True
        if self.inside:
            if tag == 'id':  # heurist unique id (H-ID)
                self.attrs = {'hid': e.text, }
            elif tag == 'type':
                self.datatype = e.attrib['id']
            elif tag == 'detail':
                field_id = e.attrib['conceptID']
                field_values = self.attrs.get(field_id, list())
                field_values.append(e.text)
                self.attrs[field_id] = field_values
            elif tag == 'relationship':  # only for keywords
                field_id = e.attrib['termID']
                field_values = self.attrs.get(field_id, list())
                field_values.append(e.attrib['relatedRecordID'])
                self.attrs[field_id] = field_values
        return None

    def end(self, e):
        tag = Record.get_tag(e)
        if tag == self.TAG:
            self.inside = False
            heurist_id = self.attrs['hid']
            records_map = self.records.get(self.datatype, dict())
            records_map[heurist_id] = self.attrs
            self.records[self.datatype] = records_map
            self.reset()
        return None

    def reset(self):
        self.datatype = None
        self.attrs = None

    @staticmethod
    def get_tag(e):
        return e.tag
