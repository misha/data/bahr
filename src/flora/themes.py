# -*- coding: utf-8 -*-


class Theme(object):

    FIELDS = [
            'key', 'create_user', 'create_date', 'modify_user', 'modify_date',
            'Descripteur','Synonymes',
            ]
    FLORA = {
        'UNIQUE_KEY': 'key',
        'CREATE_USER': 'create_user',
        'CREATE_DATE': 'create_date',
        'MODIFY_USER': 'modify_user',
        'MODIFY_DATE': 'modify_date',
        'FIELD1': 'descripteur',
        'EQUIV': 'synonymes',
        }
    TAG = 'TODO'


    def start(self, e):
        if e.tag in self.FLORA.keys():
            attr = self.FLORA[e.tag]
            value = e.text
            if attr == 'synonymes':
                l = getattr(self, attr, [])
                l.append(value)
                setattr(self, attr, l)
            else:
                setattr(self, attr, value)
        return None


    def end(self, e):
        if e.tag == self.TAG:
            fields = ['key',
                'create_user', 'create_date', 'modify_user', 'modify_date',
                'descripteur', 'synonymes'
                ]
            values = []
            for attr in fields:
                if attr == 'synonymes':
                    l = getattr(self, attr, '')
                    values.append('|'.join(l))
                else:
                    values.append(getattr(self, attr, ''))
            csv = ','.join('"{0}"'.format(w) for w in values)
            self.reset()
            return csv
        return None


    def reset(self):
        for attr in self.FLORA.values():
            if hasattr(self, attr):
                delattr(self, attr)


class Anthroponyme(Theme):

    TAG = 'BAHR_ANTH'


class Source(Theme):

    TAG = 'BAHR_SRC'

class People(Theme):

    TAG = 'BAHR_PEOPLE'

class GeoName(Theme):

    TAG = 'BAHR_GEO'
