# -*- coding: utf-8 -*-


class Revue(object):

    FIELDS = [
            'Revue H-ID',
            'key', 'create_user', 'create_date', 'modify_user', 'modify_date',
            'Nom','Lieu','Abbréviation','Historique','ISSN','Imprimé',
            ]
    FLORA = {
        'UNIQUE_KEY': 'key',
        'CREATE_USER': 'create_user',
        'CREATE_DATE': 'create_date',
        'MODIFY_USER': 'modify_user',
        'MODIFY_DATE': 'modify_date',
        'FIELD1': 'abbreviation',
        'FIELD2': 'nom',
        'FIELD2x': 'lieu',
        'FIELD3': 'historique',
        'FIELD4': 'issn',
        'FIELD5': 'imprime',
        }
    TAG = 'BAHR_REVUE'


    def start(self, e):
        if e.tag in self.FLORA.keys():
            attr = self.FLORA[e.tag]
            value = e.text
            if attr == 'nom':
                parts = value.split(' - ')
                #if len(parts) != 2:
                #    print('nom: "%s"' % value)
                #else:
                #    print('%s: %s' % (self.key, parts[1]))
                setattr(self, 'nom', parts[0])
                if len(parts) > 1:
                    setattr(self, 'lieu', parts[-1])
                if len(parts) > 2:
                    raise ValueError('Génération nom+lieu: "%s"[%s]' % (value, self.key))
            else:
                setattr(self, attr, value)
        return None


    def end(self, e):
        if e.tag == self.TAG:
            fields = ['key', 'key',
                'create_user', 'create_date', 'modify_user', 'modify_date',
                'nom', 'lieu', 'abbreviation', 'historique', 'issn', 'imprime'
                ]
            values = []
            for attr in fields:
                values.append(getattr(self, attr, ''))
            csv = ','.join('"{0}"'.format(w) for w in values)
            self.reset()
            return csv
        return None

    def reset(self):
        for attr in self.FLORA.values():
            if hasattr(self, attr):
                delattr(self, attr)


class OuvrageCollectif(object):

    FLORA = {
        'UNIQUE_KEY': 'key',
        'CREATE_USER': 'create_user',
        'CREATE_DATE': 'create_date',
        'MODIFY_USER': 'modify_user',
        'MODIFY_DATE': 'modify_date',
        'FIELD1': 'titre',
        'FIELD2': 'auteurs',
        'FIELD3': 'collection',
        'FIELD4': 'tome',
        'FIELD5': 'millesime',
        'FIELD6': 'pages',
        }
    FIELDS = FLORA.values()
    TAG = 'BAHR_OC'

    def start(self, e):
        if e.tag in self.FLORA.keys():
            attr = self.FLORA[e.tag]
            value = e.text
            if attr == 'auteurs':
                value = '|'.join(value.split('_RS_'))
            setattr(self, attr, value)
        return None


    def end(self, e):
        if e.tag == self.TAG:
            fields = self.FIELDS
            values = []
            for attr in fields:
                values.append(getattr(self, attr, ''))
            csv = ';'.join('{0}'.format(w) for w in values)
            self.reset()
            return csv
        return None

    def reset(self):
        for attr in self.FLORA.values():
            if hasattr(self, attr):
                delattr(self, attr)


class Collection(object):

    FLORA = {
        'UNIQUE_KEY': 'key',
        'CREATE_USER': 'create_user',
        'CREATE_DATE': 'create_date',
        'MODIFY_USER': 'modify_user',
        'MODIFY_DATE': 'modify_date',
        'FIELD1': 'title',
        }
    FIELDS = FLORA.values()
    TAG = 'BAHR_COLL'


    def start(self, e):
        if e.tag in self.FLORA.keys():
            attr = self.FLORA[e.tag]
            value = e.text
            setattr(self, attr, value)
        return None


    def end(self, e):
        if e.tag == self.TAG:
            fields = self.FIELDS
            values = []
            for attr in fields:
                values.append(getattr(self, attr, ''))
            csv = ','.join('"{0}"'.format(w) for w in values)
            self.reset()
            return csv
        return None


    def reset(self):
        for attr in self.FLORA.values():
            if hasattr(self, attr):
                delattr(self, attr)
