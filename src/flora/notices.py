# -*- coding: utf-8 -*-

SOURCES_REFS = {}
SOURCES_DUPS = [ 0 ]
NOTICES_COUNT = {
        '2016-': 0,
        '2017': 0,
        '2018': 0,
        '2019': 0,
        '2020': 0,
        '2021': 0,
        }


class Notice(object):

    FLORA = {
        'UNIQUE_KEY': 'key',
        'CREATE_USER': 'create_user',
        'CREATE_DATE': 'create_date',
        'MODIFY_USER': 'modify_user',
        'MODIFY_DATE': 'modify_date',
        'ETAT': 'etat',
#        'SCENARIO': 'scenario',  # bahr_rev_scenario|bahr_oc_scenario, dropped
#        'TD': 'type',  # revue|ouvrage collectif ; dropped
        'AUTEUR': 'auteurs', # --> BAHR_AUTEUR
        'TI': 'titre',
        'TI_ROLE': 'role',
        # <REVUE>
        'ABREV': 'revue',  # --> BAHR_REVUE
        'TOME': 'tome',
        'FASC': 'fascicule',
        'DATE': 'millesime',
        'PAGES': 'pages',
        # </REVUE>
        # <OC>
        'OC_FIELD1': 'pages',
        # </OC>
        'OC_TI': 'ouvrage_collectif',  # --> BAHR_OC
        'URL': 'url', # we won't bother with the default value 'http://'
        'LNG_ART': 'langage_article',
        'LNG_RES': 'langage_abstract',
        'NOTES': 'notes',
        'CHDEB': 'chronology_begin',
        'CHFIN': 'chronology_end',  # can be an int or 'xx+'
        'PERIODE': 'period',  # period.lst becames a Record
        'ANTH': 'anthroponymes',  # --> BAHR_ANTH
        'PEOPLE': 'peuples',  # --> BAHR_PEOPLE
        'GEO': 'geonames',  # --> BAHR_GEO
        'SRC': 'sources',  # @see convert.write_sources_csv
        'MCL_THES_MCL': 'keywords',
#        'MCL_CAND': None,  # TODO add to keywords
#        'M_LIB': 'tags',     # nearly unused, dropped
#        'REVERS': None,   # unused, dropped
        'RES': 'summary',
        'RES_OPT': 'res_opt',  # wtf?
        'SIGN': 'signature',  # BAHR_SIG
        }
    FIELDS = list(FLORA.values()) + ['published_in']
    TAG = 'BAHR'

    ETATS = { '0': 'Brut', '1': 'En cours', '2': 'Validé', '3': 'Publié' }
    ROLES = { '1': 'Normal', '2': 'Anonyme' }


    def start(self, e):
        if e.tag in self.FLORA.keys():
            attr = self.FLORA[e.tag]
            value = e.text
            if attr == 'etat':
                xkey = int(self.key)
                if 62454 <= xkey <= 69061:
                    # 2019: [ 67181 ; 69061 ]
                    # 2018: [ 64825 ; 67180 ]
                    # 2017: [ 62454 ; 64824 ]
                    value = '3'
                value = self.ETATS[value]
            elif attr == 'role':
                value = self.ROLES[value]
            elif attr == 'auteurs' or \
               attr == 'signature' or \
               attr == 'anthroponymes' or \
               attr == 'peuples' or \
               attr == 'geonames':
                value = '|'.join(value.split('_RS_'))
            elif attr == 'keywords':
                flora_syntax = value.split('_RS_')
                def reformat(kw):
                    PREFIX = 'bahr:THES_BAHR_MCL:'
                    if not kw.startswith(PREFIX):
                        raise SyntaxError('KEYWORD ERROR "%s"' % kw)
                    return kw[len(PREFIX):]
                keywords = [ reformat(kw) for kw in flora_syntax ]
                value = '|'.join(keywords)
            elif attr == 'url' and value == 'http://':
                value = None
            elif attr == 'langage_article' or attr == 'langage_abstract':
                languages = e.attrib['display']
                value = '|'.join(languages.split('_RS_'))
            elif attr == 'chronology_begin' or value == 'chronology_end':
                value = value.replace(' ', '')
            elif attr == 'period':
                value = '|'.join(value.replace(' ','').split('/'))
            elif attr == 'sources':
                # this SRC element will become Reference record in Heurist
                value = None  # we shouldn't need this, but just in case
                #ref = {'source': None, 'reference': None }
                ref = (None, None)  # (source, reference)
                for child in e.iterdescendants():
                    if child.tag == 'SRC_MOT':
                        # UNIQUE_KEY of BAHR_SRC, hopefuly
                        ref = (child.text, ref[1])
                    elif child.tag == 'S_REF':
                        # something researchers in the field
                        # of roman history understand, I guess
                        refs = child.text.split(';')
                        for part in refs:
                            # remove surrounding spaces (trim)
                            r = part.strip()
                            # merge multiple spaces into one
                            r = ' '.join(r.split())
                            self.add_source_reference((ref[0], r))
            elif attr == 'summary' or attr == 'notes' or attr == 'titre':
                value = value.replace('"','\\"')
                # Heurist doesn't like newlines during .csv import
                # thus, we use this silly magic placeholder ; then, in Heurist,
                # you'll have to replace back stuff
                value = value.replace('\n',' %THISWASANEWLINE% ')
            elif attr == 'res_opt':
                value = 'Non' if value == '0' else 'Oui'
            if value:
                setattr(self, attr, value)
                #print("'%s'='%s'" % (attr, value))
        return None


    def end(self, e):
        if e.tag == self.TAG:
            # 2020: [ 69062 ; 70000 ]
            # 2019: [ 67181 ; 69061 ]
            # 2018: [ 64825 ; 67180 ]
            # 2017: [ 62454 ; 64824 ]
            xkey = int(self.key)
            if xkey <= 62453:
                NOTICES_COUNT['2016-']+= 1
                setattr(self, 'published_in', '2016-')
            elif 62454 <= xkey <= 64824:
                NOTICES_COUNT['2017'] += 1
                setattr(self, 'published_in', '2017')
            elif 64825 <= xkey <= 67180:
                NOTICES_COUNT['2018'] += 1
                setattr(self, 'published_in', '2018')
            elif 67181 <= xkey <= 69061:
                NOTICES_COUNT['2019'] += 1
                setattr(self, 'published_in', '2019')
            elif 69062 <= xkey <= 70000:
                NOTICES_COUNT['2020'] += 1
                setattr(self, 'published_in', '2020')
            else:
                NOTICES_COUNT['2021'] += 1
            values = []
            for attr in self.FIELDS:
                value = getattr(self, attr, '')
                if isinstance(value, list):
                    if len(value) > 36:
                        # Heurist seems to stall silently in this case
                        print("WARNING: %s:'%s' too long (%s)" % (self.key, attr, len(value)))
                    value = '|'.join(str(e) for e in value)
                elif isinstance(value, str):
                    if '_RS_' in value:
                        print("WARNING: %s:'%s'='%s'" % (self.key, attr, value))
                values.append(value)
            csv = ','.join('"{0}"'.format(w) for w in values)
            self.reset()
            return csv
        return None


    def reset(self):
        for attr in self.FIELDS:
            if hasattr(self, attr):
                delattr(self, attr)

    def add_source_reference(self, ref):
        try:
            index = list(SOURCES_REFS.values()).index(ref)
            SOURCES_DUPS[0] += 1
        except ValueError:  # first time ref is encountered
            index = 57147+len(SOURCES_REFS)
            SOURCES_REFS[index] = ref
        try:
            if index not in self.sources:
                self.sources.append(index)
        except AttributeError:
            setattr(self, 'sources', [index, ])
