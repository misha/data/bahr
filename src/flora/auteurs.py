# -*- coding: utf-8 -*-


class Auteur(object):

    FIELDS = [
            'key', 'create_user', 'create_date', 'modify_user', 'modify_date',
            'Nom','Prénom',
            ]
    FLORA = {
        'UNIQUE_KEY': 'key',
        'CREATE_USER': 'create_user',
        'CREATE_DATE': 'create_date',
        'MODIFY_USER': 'modify_user',
        'MODIFY_DATE': 'modify_date',
        'FIELD1': 'nom',
        'FIELD1x': 'prenom',
        }


    def start(self, e):
        if e.tag in self.FLORA.keys():
            attr = self.FLORA[e.tag]
            value = e.text
            if attr == 'nom':
                prenom = value[value.find('(')+1:value.rfind(')')]
                if len(prenom) == len(value):
                    setattr(self, 'nom', value.strip())
                else:
                    nom = value.split('(')[0].strip()
                    setattr(self, 'nom', nom)
                    setattr(self, 'prenom', prenom.strip())
            else:
                setattr(self, attr, value)
        return None


    def end(self, e):
        if e.tag == 'BAHR_AUTEUR':
            fields = ['key',
                'create_user', 'create_date', 'modify_user', 'modify_date',
                'nom', 'prenom'
                ]
            values = []
            for attr in fields:
                values.append(getattr(self, attr, ''))
            csv = ','.join('"{0}"'.format(w) for w in values)
            self.reset()
            return csv
        return None


    def reset(self):
        for attr in self.FLORA.values():
            if hasattr(self, attr):
                delattr(self, attr)


class Signature(object):

    FLORA = {
        'UNIQUE_KEY': 'key',
        'CREATE_USER': 'create_user',
        'CREATE_DATE': 'create_date',
        'MODIFY_USER': 'modify_user',
        'MODIFY_DATE': 'modify_date',
        'FIELD1': 'initiales',
        'FIELD2': 'nom',
        'FIELD2x': 'prenom',
        }
    FIELDS = FLORA.values()
    TAG = 'BAHR_SIG'


    def start(self, e):
        if e.tag in self.FLORA.keys():
            attr = self.FLORA[e.tag]
            value = e.text
            if attr == 'nom':
                parts = value.split(' ')
                if len(parts) < 2:
                    prenom = ''
                    nom = parts[0]
                else:
                    prenom = parts[0]
                    nom = ' '.join(parts[1:])
                setattr(self, 'nom', nom.strip())
                setattr(self, 'prenom', prenom.strip())
            else:
                setattr(self, attr, value)
        return None


    def end(self, e):
        if e.tag == self.TAG:
            values = []
            for attr in self.FIELDS:
                values.append(getattr(self, attr, ''))
            csv = ','.join('"{0}"'.format(w) for w in values)
            self.reset()
            return csv
        return None


    def reset(self):
        for attr in self.FLORA.values():
            if hasattr(self, attr):
                delattr(self, attr)
