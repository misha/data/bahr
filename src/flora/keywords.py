# -*- coding: utf-8 -*-
from lxml import etree


def parse_tree(path):
    tree = None
    with open(path, 'r', encoding='utf-8') as fd:
        tree = etree.parse(fd)#, parser)
    return tree


def load_keywords(context):
    keywords = {}
    target = Keyword()
    for action, e in context:
        keyword = getattr(target, action)(e)
        if keyword is not None:
            key = keyword['key']
            try:
                keywords[key]
                raise ValueError("Duplicate UNIQUE_KEY '%s'" % key)
            except KeyError:  # allright, this is expected case
                keywords[key] = keyword
    print('%s keywords' % len(keywords))
    return keywords


def load_relations(context):
    relations = {}
    target = Relation()
    for action, e in context:
        relation = getattr(target, action)(e)
        if relation is not None:
            key = relation['key']
            try:
                relations[key]
                raise ValueError("Duplicate UNIQUE_KEY '%s'" % key)
            except KeyError:  # allright, this is expected case
                relations[key] = relation
    old = len(relations)
    relations = sanitize(relations)
    print('%s -> %s relations' % (old, len(relations)))
    return relations


def sanitize(relations):
    links = set()
    no_dups = dict()
    for r in relations.values():
        if (r['from'], r['to']) not in links:
            if (r['to'], r['from']) not in links:
                links.add((r['from'], r['to']))
                no_dups[r['key']] = r
    return no_dups


def write_hml(fd, context, relations_file):
    # walk args.input tree, create keywords map
    keywords = load_keywords(context)
    # parse and walk relations_file (ie. args.relations) tree,
    # create relations map
    tree = parse_tree(relations_file)
    events=('start', 'end')
    context = etree.iterwalk(tree.getroot(), events=events)
    relations = load_relations(context)

    # output everything in appropriate file
    header = '''<?xml version='1.0' encoding='UTF-8'?>
<hml xmlns="http://heuristnetwork.org" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://heuristnetwork.org/reference/scheme_hml.xsd">
<database id="0">misha_bahr</database>
<query q="{&quot;t&quot;:&quot;65&quot;}" db="misha_bahr" depth="all"/>
<dateStamp>2020-10-28T16:57:09+01:00</dateStamp>
<resultCount>%s</resultCount>
<records>''' % (len(keywords))
    fd.write(header)

    for keyword in keywords.values():
        record = Keyword.as_hml_record(keyword, relations)
        fd.write(record)

    for relation in relations.values():
        record = Relation.as_hml_record(relation, keywords)
        fd.write(record)

    footer = '''
</records>
<recordCount>%s</recordCount>
</hml>''' % (len(keywords)+len(relations))
    fd.write(footer)


class Keyword(object):

    FIELDS = [
            'key', 'create_user', 'create_date', 'modify_user', 'modify_date',
            'concept_type', 'nom', 'notes',
            ]
    FLORA = {
        'CONCEPT_ID': 'key',
        'CREATE_USER': 'create_user',
        'CREATE_DATE': 'create_date',
        'MODIFY_USER': 'modify_user',
        'MODIFY_DATE': 'modify_date',
        'CONCEPT_TYPE': 'concept_type',  # 1: descripteur ; 2: synonyme
        'TERM_FR': 'nom',
        # 'SORT_FR': 'sort_fr',
        'NOTES_FR': 'notes',
        }
    TAG = 'THES_BAHR_MCL'
    keyword = {}


    def start(self, e):
        if e.tag in self.FLORA.keys():
            attr = self.FLORA[e.tag]
            value = e.text
            self.keyword[attr] = value
            if e.tag == 'CONCEPT_TYPE':
                if value != '1' and value != '2':
                    raise LookupError("CONCEPT_TYPE '%s': Not found." % value)
        return None

    def end(self, e):
        if e.tag != self.TAG:
            return None
        result = self.keyword
        self.keyword = {}  # reset
        return result

    @staticmethod
    def as_hml_record(keyword, relations):
        types = {'1': 'Oui', '2': 'Non' }
        fields_lines = []
        key = keyword['key']
        fields_lines.append('<detail conceptID="0000-939" name="key" id="939" basename="clé">%s</detail>' % key)
        fields_lines.append('<detail conceptID="2-1" name="Descripteur" id="1" basename="Name / Title">%s</detail>' % keyword['nom'])
        value = keyword.get('concept_type', None)
        if value:
            fields_lines.append('<detail conceptID="0000-959" name="À imprimer" id="959" basename="À imprimer" termID="6249" termConceptID="99-5446" ParentTerm="Yes-No">%s</detail>' % types[value])
        value = keyword.get('create_user', None)
        if value:
            fields_lines.append('<detail conceptID="0000-946" name="create_user" id="946" basename="create_user">%s</detail>' % value)
        value = keyword.get('create_date', None)
        if value:
            fields_lines.append('<detail conceptID="0000-947" name="create_date" id="947" basename="create_date">%s</detail>' % value)
        value = keyword.get('modify_user', None)
        if value:
            fields_lines.append('<detail conceptID="0000-948" name="modify_user" id="948" basename="modify_user">%s</detail>' % value)
        value = keyword.get('modify_date', None)
        if value:
            fields_lines.append('<detail conceptID="0000-949" name="modify_date" id="949" basename="modify_date">%s</detail>' % value)

        # find relations relevant to keyword
        relations_lines = []
        def as_hml(relation):
            if relation['type'] == '1':
                term = 'Générique'
                conceptId = '6260'
            elif relation['type'] == '2':
                term = 'Spécifique'
                conceptId = '6261'
            elif relation['type'] == '3':
                term = 'Voir aussi'
                conceptId = '6262'
            elif relation['type'] == '4':
                term = 'Synonyme'
                conceptId = '6259'
            elif relation['type'] == '5':
                term = 'Utilisé pour'
                conceptId = '6263'
            else:
                return None
            return '<relationship termID="%s" relatedRecordID="%s" term="%s" termConceptID="0000-%s">%s</relationship>' % (conceptId, MAGIC(relation['to']), term, conceptId, MAGIC(relation['key'], False))
        for r in relations.values():
            if r['from'] == key:
                line = as_hml(r)
                if line is not None:
                    relations_lines.append(line)
        # create output HML record
        result = '''
<record visibility="public" visnote="no login required" selected="no" depth="0">
  <id>%s</id>
  <type id="65" conceptID="0000-65">Mot-clé</type>
  <workgroup id="1">Database Managers</workgroup>
  %s
  %s
</record>''' % (MAGIC(key), '\n  '.join(fields_lines), '\n  '.join(relations_lines))
        return result


class Relation(object):

    FIELDS = [
            'key', 'type', 'from', 'to',
            ]
    FLORA = {
        'UNIQUE_KEY': 'key',
        'RELATION_ID': 'type',
        'FROM_CONCEPT_ID': 'from',
        'TO_CONCEPT_ID': 'to',
        }
    TAG = 'THES_LINKS_BAHR_MCL'

    FLORA_TYPES = {
        '1': 'generic',   # invert of 2
        '2': 'specific',  # invert of 1
        '3': 'see also',  # invert of 5
        '4': 'synonym',
        '5': 'used for',  # invert of 4
        }
    TYPES = {
        '1': '<detail conceptID="2-6" name="Relationship type" id="6" basename="Relationship type" termID="6260" termConceptID="0000-6260" ParentTerm="Thesaurus">Générique</detail>',
        '2': '<detail conceptID="2-6" name="Relationship type" id="6" basename="Relationship type" termID="6261" termConceptID="0000-6261" ParentTerm="Thesaurus">Spécifique</detail>',
        '3': '<detail conceptID="2-6" name="Relationship type" id="6" basename="Relationship type" termID="6262" termConceptID="0000-6262" ParentTerm="Thesaurus">Voir aussi</detail>',
        '4': '<detail conceptID="2-6" name="Relationship type" id="6" basename="Relationship type" termID="6259" termConceptID="0000-6259" ParentTerm="Thesaurus">Synonyme</detail>',
        '5': '<detail conceptID="2-6" name="Relationship type" id="6" basename="Relationship type" termID="6263" termConceptID="0000-6263" ParentTerm="Thesaurus">Utilisé pour</detail>',
        }
    relation = {}


    def start(self, e):
        if e.tag in self.FLORA.keys():
            attr = self.FLORA[e.tag]
            value = e.text
            self.relation[attr] = value
            if e.tag == 'RELATION_ID':
                if value not in self.TYPES.keys():
                    raise LookupError("RELATION_ID '%s': Not found." % value)
        return None

    def end(self, e):
        if e.tag != self.TAG:
            return None
        for field in self.FIELDS:
            try:
                self.relation[field]
            except KeyError:
                print("WARNING: missing field '%s' for %s" % (field, self.relation))
                return None
        result = self.relation
        self.relation = {}  # reset
        return result

    @staticmethod
    def as_hml_record(relation, keywords):
        fields_lines = []
        rtype = Relation.TYPES[relation['type']]
        result = '''
<record visibility="public" visnote="no login required" selected="no" depth="0">
  <id>%s</id>
  <type id="1" conceptID="2-1">Record relationship</type>
  <workgroup id="1">Database Managers</workgroup>
  <detail conceptID="2-7" name="Source record" id="7" basename="Source record pointer" isRecordPointer="true">%s</detail>
  %s
  <detail conceptID="2-5" name="Target record" id="5" basename="Target record pointer" isRecordPointer="true">%s</detail>
</record>''' % (MAGIC(relation['key'], False), MAGIC(relation['from']), rtype, MAGIC(relation['to']))
        return result

def MAGIC(key, kw=True):
    if kw:
        return str(int(key)+144000)
    return str(int(key)+101010)
