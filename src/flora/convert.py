# -*- coding: utf-8 -*-
import argparse
from lxml import etree
from revues import Revue, OuvrageCollectif, Collection
from auteurs import Auteur, Signature
from themes import *
from keywords import write_hml
from notices import Notice


MODELS = {
        'revue': Revue,
        'oc': OuvrageCollectif,
        'collec': Collection,
        'auteur': Auteur,
        'anthro': Anthroponyme,
        'source': Source,
        'people': People,
        'geo': GeoName,
        'kw': None,  # keywords.write_hml manage its own targets
        'notice': Notice,
        'signature': Signature,
        }


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', nargs='+',
            help="input file path (Open Flora XML file)")
    parser.add_argument('-o', '--output', default='output.csv',
            help="output filename")
    parser.add_argument('-m', '--model', required=True, choices=MODELS.keys(),
            help="Type of model to parse")
    parser.add_argument('-t', '--no-header', action='store_true',
            help="If true, don't begin by writing header")
    parser.add_argument('-r', '--relations',
            help="Path to thesaurus relations file that will be read.\
                  Only used if model='kw'.")
    parser.add_argument('-s', '--sources', default='sources_refs.csv',
            help="Path to antique references file that will be written.\
                  Only used if model='notice'.")
    return parser


def parse_tree(path):
    tree = None
    with open(path, 'r', encoding='utf-8') as fd:
        tree = etree.parse(fd)#, parser)
    return tree


def write_csv(fd, context, target):
    for action, e in context:
        line = getattr(target, action)(e)
        if line is not None:
            fd.write('%s\n' % line)


def write_sources_csv(fd, refs):
    header = '"index","source","references"'
    fd.write('%s\n' % header)
    for index, ref in refs.items():
        line = '"%s","%s","%s"' % (index, ref[0], ref[1])
        fd.write('%s\n' % line)
    print('"unique" sources: %s' % (len(refs)))


def parse(args, target_class):

    if len(args.input) > 0 and not args.no_header and target_class is not None:
        with open(args.output, 'a') as fd:
            header = ','.join('"{0}"'.format(w) for w in target_class.FIELDS)
            fd.write('%s\n' % header)

    for path in args.input:
        tree = parse_tree(path)
        events = ('start', 'end')
        context = etree.iterwalk(tree.getroot(), events=events)
        with open(args.output, 'a') as fd:
            if args.model != 'kw':
                target = target_class()
                print('file: %s ; target: %s' % (path, type(target).__name__))
                write_csv(fd, context, target)
            else:
                write_hml(fd, context, args.relations)

    if args.model == 'notice':
        with open(args.sources, 'a') as fd:
            from notices import SOURCES_REFS, SOURCES_DUPS
            write_sources_csv(fd, SOURCES_REFS)
            print('duplicates removed: %s' % (SOURCES_DUPS[0]))


if __name__ == '__main__':
    args = create_parser().parse_args()
    target = MODELS[args.model]
    parse(args, target)
