# -*- coding: utf-8 -*-
from unittest import TestCase
from parameterized import parameterized
from ..roman import roman2int, int2roman


class TestRomans(TestCase):

    @parameterized.expand([
        (1, 'I'), (2, 'II'), (3, 'III'), (4, 'IV'), (5, 'V'),
        (6, 'VI'), (7, 'VII'), (8, 'VIII'), (9, 'IX'), (10, 'X'),
        (11, 'XI'), (12, 'XII'), (13, 'XIII'), (14, 'XIV'), (15, 'XV'),
        (16, 'XVI'), (17, 'XVII'), (18, 'XVIII'), (19, 'XIX'), (20, 'XX'),
        (21, 'XXI'), (22, 'XXII'), (23, 'XXIII'), (24, 'XXIV'), (25, 'XXV'),
        (26, 'XXVI'), (27, 'XXVII'), (28, 'XXVIII'), (29, 'XXIX'), (30, 'XXX'),
        (31, 'XXXI'), (32, 'XXXII'), (33, 'XXXIII'), (34, 'XXXIV'),
        (35, 'XXXV'), (36, 'XXXVI'), (37, 'XXXVII'), (38, 'XXXVIII'),
        (39, 'XXXIX'), (40, 'XL'),
        (41, 'XLI'), (42, 'XLII'), (43, 'XLIII'), (44, 'XLIV'), (45, 'XLV'),
        (46, 'XLVI'), (47, 'XLVII'), (48, 'XLVIII'), (49, 'XLIX'), (50, 'L'),
        (51, 'LI'), (52, 'LII'), (53, 'LIII'), (54, 'LIV'), (55, 'LV'),
        (56, 'LVI'), (57, 'LVII'), (58, 'LVIII'), (59, 'LIX'), (60, 'LX'),
        (61, 'LXI'), (62, 'LXII'), (63, 'LXIII'), (64, 'LXIV'), (65, 'LXV'),
        (66, 'LXVI'), (67, 'LXVII'), (68, 'LXVIII'), (69, 'LXIX'), (70, 'LXX'),
        (71, 'LXXI'), (72, 'LXXII'), (73, 'LXXIII'), (74, 'LXXIV'),
        (75, 'LXXV'), (76, 'LXXVI'), (77, 'LXXVII'), (78, 'LXXVIII'),
        (79, 'LXXIX'), (80, 'LXXX'),
        (81, 'LXXXI'), (82, 'LXXXII'), (83, 'LXXXIII'), (84, 'LXXXIV'),
        (85, 'LXXXV'), (86, 'LXXXVI'), (87, 'LXXXVII'), (88, 'LXXXVIII'),
        (89, 'LXXXIX'), (90, 'XC'),
        (91, 'XCI'), (92, 'XCII'), (93, 'XCIII'), (94, 'XCIV'), (95, 'XCV'),
        (96, 'XCVI'), (97, 'XCVII'), (98, 'XCVIII'), (99, 'XCIX'), (100, 'C'),
        # and son on ...
        ])
    def test_first_hundred(self, expected, value):
        self.assertEqual(expected, roman2int(value))

    @parameterized.expand([
        (4, 'IIII'), (5, 'IIIII'),
        (40, 'XXXX'), (50, 'XXXXX'),
        (400, 'CCCC'), (500, 'CCCCC'),
        (4000, 'MMMM'), (5000, 'MMMMM'),
        ])
    def test_additive_notation(self, expected, value):
        with self.assertRaises(ValueError):
            # in the ValueError message, you get a suggestion :
            # eg. 'IV' instead of 'IIII' ; but I won't bother to test it
            roman2int(value)

    @parameterized.expand([
        (499, 'CDXCIX')
        ])
    def test_499(self, expected, value):
        self.assertEqual(expected, roman2int(value))

    @parameterized.expand([
        (499, 'ID'), (499, 'VDIV'), (499, 'XDIX'), (499, 'LDVLIV'),
        ])
    def test_499_irregular_substractive_notation(self, expected, value):
        with self.assertRaises(ValueError):
            # in the ValueError message, you get a suggestion : f
            # eg. 'CDXCIX' instead of all these values
            roman2int(value)

    @parameterized.expand([
        'HELLO', 'WORLD',
        ])
    def test_not_roman_numerals(self, value):
        with self.assertRaises(ValueError):
            roman2int(value)

    @parameterized.expand([
        (42, ), (3.14, ),
        ])
    def test_roman2int_type_error(self, value):
        with self.assertRaises(TypeError):
            roman2int(value)

    @parameterized.expand([
        ('icanhaz', ), (3.14, ),
        ])
    def test_int2roman_type_error(self, value):
        with self.assertRaises(TypeError):
            int2roman(value)
