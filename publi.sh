#!/bin/bash


create_draft() {
  input=$1
  output=$2
  cp $input $output
  tmp=$output.tmp
  # remove paged.js usage
  sed -E "s/<script src=\".+paged.*js\"><\/script>//g" $output > $tmp && mv $tmp $output
  # display lexicons on a single colon
  sed "s/column-count: 2;//g" $output > $tmp && mv $tmp $output
}

year=$1
volume=$2
envname=EXPORT_20$year
notices=resources/hml/Notices20$year.xml
output=public/BAHR20${year}.html
simple=public/BAHR20${year}_simple.html

rm -f $notices $output $simple
mkdir -p resources/hml public
wget -q -O $notices https://seafile.unistra.fr/f/${!envname}/?dl=1
python3 src/publish/convert.py $notices -t src/publish/template.html -n $volume -y $year -l -o $output --verbose
create_draft $output $simple
