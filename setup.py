from setuptools import setup, find_packages

setup(
    name='bahr-flora-to-heurist',
    version='1.0.0',
    url='https://gitlab.com/vous/fizzbuzz.git',
    author='Régis Witz',
    author_email='rwitz@unistra.fr',
    description='Conversion de la base BAHR depuis Open Flora vers Heurist',
    packages=find_packages(),
)
